import React from "react";
import axios from 'axios';
import { toast } from "react-toastify";

class CollectClinicals extends React.Component{
state={}
componentWillMount(){
axios.get('http://localhost:8080/app/api/patients/'+this.props.match.params.patientId).then(res=>{

	this.setState(res.data);

})

	}

	handleSubmit(event){
		event.preventDefault();
		const data={
			patientId:this.props.match.params.patientId,
			componentName:this.componentName,
			componentValue:this.componentValue
		}
		axios.post('http://localhost:8080/app/api/clinicals/',data).then(res=>{
			toast("Patient Data save succesfully",{autoClose:3000,position:toast.POSITION.BOTTOM_CENTER});
		})
	}
	render(){
		return(<div>
			<h2> Patient Detail :</h2>
			First name:{this.state.firstName}
			Last name:{this.state.lastName}
			Age:{this.state.age}
			<h2> Clinical Data :</h2>
			<form>
			clinical Entry Type<select onChange={(event)=>{this.componentName=event.target.value}}>
			<option>Select One</option>
			<option value="bp">Blood Pressure</option>
			<option value="hw">Height/weight</option>
			<option value="heartRate">Heart Rate</option>
			</select>
			Value: <input type="text" name="componentValue" onChange={(event)=>{this.componentValue=event.target.value}}/>
			<button onClick={this.handleSubmit.bind(this)}>Confirm</button>

			</form>



			</div>)
	}
	
}
export default CollectClinicals;