import React from "react";
import {Link} from 'react-router-dom';
import axios from 'axios';
import {toast} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

toast.configure();


class AddPatient extends React.Component{

handleSubmit(event){
	event.preventDefault();
	const data={
		firstName:this.firstName,
		lastName:this.lastName,
		age:this.age
	}
	axios.post('http://localhost:8080/app/api/patients',data).then(res=>{
		toast("Patient added Successfully!",{autoClose:2000,position:toast.POSITION.BOTTOM_CENTER})
		
	})
}

	render(){
	return(<div>  
		<h1>Create Patient:</h1>
<form>
FirstName: <input type="text" name="firstName"  onChange={(event=>this.firstName=event.target.value)}/>
LastName:<input type="text" name="lastName" onChange={(event=>this.lastName=event.target.value)}/>
Age:<input type="text" name="age" onChange={(event=>this.age=event.target.value)}/>
<button onClick={this.handleSubmit.bind(this)}>Confirm</button>
</form>
<Link to={'/'}>Go Back</Link>
	</div>)
	}
}
export default AddPatient;
