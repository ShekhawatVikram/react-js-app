import React from 'react';
import logo from './logo.svg';
import {Route,Switch} from "react-router-dom" ;

import Home from './component/Home';
import AddPatient from './component/AddPatient';
import CollectClinicals from './component/CollectClinicals';
import AnalyzeData from './component/AnalyzeData';
import ChartGenerator from './component/ChartGenerator';

import './App.css';

function App() {
  return (
    <div className="App">
      <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/addPatient" component={AddPatient} />
      <Route exact path="/patientDetails/:patientId" component={CollectClinicals} />
      <Route exact path="/analyze/:patientId" component={AnalyzeData} />
      <Route exact path="/chart/:componentName/:patientId" component={ChartGenerator} />
      </Switch>
    </div>
  );
}

export default App;


